### If baking chocolate chip cookies, e.g., what is the probability of seeing a chip in the dough? 

Proportional to _nv/V_, where _n_ is number of chips with volume _v_, and _V_ is total cookie volume.

For simplification... assume chip and cookie are both spherical. Let cookie radius R be unitary.
With spherical coordinates: sphere is symmetric in phi and theta, this only need to simulate radius.

Generate _R + r_ over varying number of chips and chip radii.

![PLOT](VisibilityProbability.png)

Looks like a rectangular hyperbola. This plot should probably be log-log but I could not get it to display adequately.
Also, to improve realism, the cookie should at least be a short cylinder...; the chips may remain spherical.
