import numpy as np
import math
import random
import matplotlib.pyplot as plt
import seaborn as sns
from mpl_toolkits import mplot3d


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
# Redefine pi to something shorter
pi = np.pi




### If baking chocolate chip cookies, e.g., what's the probability of seeing a 
# chip in the dough? Proportional to nv/V, where n is number of chips with 
# volume v, in total cookie volume V?

# Spherical coordinates. And since sphere is symmetric in phi, theta, only 
# need to simulate r... Cookie centered at 0; Cookie radius.
R0 = 0.
R = R0 + 1.

# Number of chips
minchips = 1
maxchips = 101
nchips = range(minchips, maxchips+1)

# Generate random radii within R plus chip radius. 
# Then iterate over some r range.
iters = 100
step = 0.01
radii = np.arange(0,R+step, step)
# Radii[r][n][i]
Radii = [[np.random.rand(iters,n) + r for n in nchips] for r in radii]


### If any radius is over R, make true (==1). Take average.
# VisibleProb[r][n]
VisibleProb = [[np.any(Radii[r][n-1] >= 1., axis=1).sum()/iters for n in nchips] for r in range(len(radii))]


### Plot it
plt.figure()
plt.imshow(VisibleProb, cmap='hot')
plt.title('N Spherical Chips in Spherical Cookie of Radius R')
plt.gca().invert_yaxis()
plt.xlabel('N')
plt.ylabel('Chip Radius (%R)')
plt.colorbar().set_label('Visibility probability')

plt.savefig('VisibilityProbability.png')
plt.show(block=False)


#ax = sns.heatmap(VisibleProb)
#ax.invert_yaxis()
#plt.show()

