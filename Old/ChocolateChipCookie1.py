import numpy as np
import math
import random
import matplotlib.pyplot as plt
import seaborn as sns


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
# Redefine pi to something shorter
pi = np.pi




### If baking chocolate chip cookies, e.g., what's the probability of seeing a chip in the dough?
# Proportional to nv/V, where n is number of chips with volume v, in total cookie volume V?


### OOO (order of operation):
# S cookie sphere.
# For nchips:
#   set random chip center < cookie surface
#   set chip sphere around chip center
# Check if chip surface is >= cookie surface


### No visualization; spherical coordinates. And since sphere is symmetric in phi, theta, only need to simulate r...
# Cookie centered at 0.
R0 = 0.

# Cookie
R = R0 + 1.

# Chips
r = 0.1*R





'''
### Do single iteration of some simuation. Single simuation of one chip, then single simulation of 2 chips, ...
maxchips = 10
nchips = range(1,maxchips+1)

# Max chip radii
Radii = []

# Loop through n chips (1, ..., maxchips). For each n, set random radius + chip radius. 
# Store as array of form [ (n=1, [r1]), (n=2, [r1,r2]), ... ]
for n in nchips:
    radii = []
    for chip in range(n):
        radius = random.uniform(0,R) + r
        radii.append(radius)
    Radii.append( (n,radii) )
'''





### Do it better way: Simulate a single chip x times. Then simulate 2 chips x times, ...
'''
iters = 100
maxchips = 10
nchips = range(1,maxchips+1)

for n in nchips:
    radii = []
    
    #radii.append(np.random.rand + r)

radii.append(random.uniform(0,R) + r)
    Radii.append( (n,radii))
'''


minchips = 1
maxchips = 101
nchips = range(minchips, maxchips+1)

iters = 100
#Radii = [[np.random.rand(1,n)+r for i in range(iters)] for n in nchips]
# Generate random radii within R (coincidentally...) plus chip radius. Shape is then Radii[nchips][iteration]
Radii = [np.random.rand(iters,n) + r for n in nchips]

RadiiAvg = [np.mean(Radii[n-1],axis=0) for n in nchips] 
# axis=0 averages all chips of single iter; axis=1 averages all iter of single chip.
# ie.
# Radii[2] = 
# array([[0.1611942 , 0.14415868, 0.74459482], 
#        [0.44774123, 0.19880623, 0.22238625]])
# np.mean(Radii[2],0) = 
# array([0.30446772, 0.17148245, 0.48349053])
# np.mean(Radii[2],1) = 
# array([0.34998256, 0.28964457])



#VisibleProb = [np.mean((Radii[n-1] > 1.).sum(axis=1)/n) for n in nchips]

# If any chips in a single iteration are above one (True), mark that iter true. Repeat for all Iters. Sum and div by iters for prob.
#VisibleProb = [np.any(Radii[n-1] >1., axis=1).sum()/iters for n in nchips]
#print(VisibleProb)





# Iter over r, too...
# Radii[r][n][iter]
step = 0.01
radii = np.arange(0,R+step, step)
Radii = [[np.random.rand(iters,n) + r for n in nchips] for r in radii]


# VisibleProb[r][
VisibleProb = [[np.any(Radii[r][n-1] >= 1., axis=1).sum()/iters for n in nchips] for r in range(len(radii))]
#print(VisibleProb)

plt.imshow(VisibleProb)#, cmap='')
#plt.imshow(np.asarray(VisibleProb[::-1]).T)
#plt.imshow(np.flip(VisibleProb, axis=1))



#img = np.array(VisibleProb)
#imgplot = plt.imshow(img)

#lum_img = img[:,:]
#plt.imshow(lum_img, cmap='hot')

#imgplot = plt.imshow(lum_img)
#plt.colorbar()

vp = np.array(VisibleProb)
plt.imshow(vp, cmap='hot')

plt.title('N Spherical Chips in Spherical Cookie of Radius R')# as a Function of Chip Size and Number of Chips')
plt.gca().invert_yaxis()
plt.xlabel('N')
plt.ylabel('Chip Radius (%R)')
cbar = plt.colorbar()
cbar.set_label('Visibility probability')
#plt.show()

plt.savefig('VisibilityProbability.pdf')
plt.show()


# Simulated a single chip. Looks like it's ~r% that a single spherical chip of r radius is visibile in a spherical cookie of R radius.
# n=10000 
# list = np.random.rand(1,n)+r
#   1. len(list[list>1.])/n
#   2. (list>1.).sum()/n  <-- faster https://stackoverflow.com/questions/12995937/count-all-values-in-a-matrix-greater-than-a-value


'''
CookieShape = 'sphere'
chipShape = 'sphere'

nChips = 1


if CookieShape = 'sphere':
    # Define unit circle parameters centered at (0,0).
    R = 1.                                                          # Radius of detector
    X0 = 0.
    Y0 = 0.
    Z0 = 0.


if chipShape = 'sphere'
    r = 0.1*R
    x0 = 0.
    y0 = 0.
    z0 = 0.
'''

